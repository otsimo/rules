"""Rules to build C targets.

These are just wrappers around the C++ rules; the logic of the two is identical
other than using different config settings.

Both C and C++ can be mixed and matched arbitrarily, but of course to do so
you must pay attention to interoperability when needed (e.g. 'extern "C"' and so forth).
"""

load("./cc_rules.star", "cc_binary", "cc_embed_binary", "cc_library", "cc_object", "cc_shared_object", "cc_static_library", "cc_test")

def c_library(
        name,
        srcs = [],
        hdrs = [],
        private_hdrs = [],
        deps = [],
        visibility = None,
        test_only = False,
        testonly = None,
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        pkg_config_libs = [],
        pkg_config_cflags = [],
        includes = [],
        defines = [],
        alwayslink = False):
    """Generate a C library target.

    Args:
      name (str): Name of the rule
      srcs (list): C source files to compile.
      hdrs (list): Header files. These will be made available to dependent rules, so the distinction
                   between srcs and hdrs is important.
      private_hdrs (list): Header files that are available only to this rule and not exported to
                           dependent rules.
      deps (list): Dependent rules.
      visibility (list): Visibility declaration for this rule.
      test_only (bool): If True, is only available to other test rules.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker; these will not be used here but will be
                           picked up by a c_binary or c_test rule.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config. Again, the ldflags
                              will be picked up by cc_binary or cc_test rules.
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`.
      includes (list): List of include directories to be added to the compiler's path.
      defines (list | dict): List of tokens to define in the preprocessor.
                             Alternatively can be a dict of name -> value to define, in which case
                             values are surrounded by quotes.
      alwayslink (bool): If True, any binaries / tests using this library will link in all symbols,
                         even if they don't directly reference them. This is useful for e.g. having
                         static members that register themselves at construction time.
    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if testonly != None:
        test_only = testonly
    return cc_library(
        name = name,
        srcs = srcs,
        hdrs = hdrs,
        private_hdrs = private_hdrs,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        includes = includes,
        defines = defines,
        alwayslink = alwayslink,
        _c = True,
    )

def c_object(
        name,
        src,
        hdrs = [],
        private_hdrs = [],
        out = None,
        test_only = False,
        testonly = None,
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        pkg_config_libs = [],
        pkg_config_cflags = [],
        includes = [],
        defines = [],
        alwayslink = False,
        visibility = None,
        deps = []):
    """Generate a C object file from a single source.

    N.B. This is fairly low-level; for most use cases c_library should be preferred.

    Args:
      name (str): Name of the rule
      src (str): C or C++ source file to compile. This can be another rule, but if so it must
                 have exactly one output.
      hdrs (list): Header files. These will be made available to dependent rules, so the distinction
                   between srcs and hdrs is important.
      private_hdrs (list): Header files that are available only to this rule and not exported to
                           dependent rules.
      out (str): Name of the output file. Defaults to name + .o.
      deps (list): Dependent rules.
      visibility (list): Visibility declaration for this rule.
      test_only (bool): If True, is only available to other test rules.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker; these will not be used here but will be
                           picked up by a c_binary or c_test rule.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config. Again, the ldflags
                              will be picked up by cc_binary or cc_test rules.
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`. Again, the ldflags
                              will be picked up by cc_binary or cc_test rules.
      includes (list): List of include directories to be added to the compiler's path.
      defines (list | dict): List of tokens to define in the preprocessor.
                             Alternatively can be a dict of name -> value to define, in which case
                             values are surrounded by quotes.
      alwayslink (bool): If True, any binaries / tests using this library will link in all symbols,
                         even if they don't directly reference them. This is useful for e.g. having
                         static members that register themselves at construction time.
    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if testonly != None:
        test_only = testonly
    return cc_object(
        name = name,
        src = src,
        hdrs = hdrs,
        private_hdrs = private_hdrs,
        out = out,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        includes = includes,
        defines = defines,
        alwayslink = alwayslink,
        _c = True,
    )

def c_static_library(
        name,
        srcs = [],
        hdrs = [],
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        deps = [],
        visibility = None,
        test_only = False,
        testonly = None,
        pkg_config_libs = [],
        pkg_config_cflags = []):
    """Generates a C static library (.a).

    This is essentially just a collection of other c_library rules into a single archive.
    Optionally this rule can have sources of its own, but it's quite reasonable just to use
    it as a collection of other rules.

    Args:
      name (str): Name of the rule
      srcs (list): C or C++ source files to compile.
      hdrs (list): Header files.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker.
      deps (list): Dependent rules.
      visibility (list): Visibility declaration for this rule.
      test_only (bool): If True, is only available to other test rules.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`

    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if testonly != None:
        test_only = testonly
    return cc_static_library(
        name = name,
        srcs = srcs,
        hdrs = hdrs,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        _c = True,
    )

def c_shared_object(
        name,
        srcs = [],
        hdrs = [],
        out = "",
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        deps = [],
        visibility = None,
        test_only = False,
        testonly = None,
        pkg_config_libs = [],
        pkg_config_cflags = [],
        includes = []):
    """Generates a C shared object (.so) with its dependencies linked in.

    Args:
      name (str): Name of the rule
      srcs (list): C or C++ source files to compile.
      hdrs (list): Header files. These will be made available to dependent rules, so the distinction
                   between srcs and hdrs is important.
      out (str): Name of the output .so. Defaults to name + .so.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker.
      deps (list): Dependent rules.
      visibility (list): Visibility declaration for this rule.
      test_only (bool): If True, is only available to other test rules.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`
      includes (list): Include directories to be added to the compiler's lookup path.
    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if testonly != None:
        test_only = testonly
    return cc_shared_object(
        name = name,
        srcs = srcs,
        hdrs = hdrs,
        out = out,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        includes = includes,
        _c = True,
    )

def c_binary(
        name,
        srcs = [],
        hdrs = [],
        private_hdrs = [],
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        deps = [],
        visibility = None,
        pkg_config_libs = [],
        pkg_config_cflags = [],
        test_only = False,
        testonly = None,
        static = False):
    """Builds a binary from a collection of C rules.

    Args:
      name (str): Name of the rule
      srcs (list): C source files to compile.
      hdrs (list): Header files.
      private_hdrs (list): Header files that are available only to this rule and not exported to
                           dependent rules.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker.
      deps (list): Dependent rules.
      visibility (list): Visibility declaration for this rule.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`
      test_only (bool): If True, this rule can only be used by tests.
      static (bool): If True, the binary will be linked statically.
    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if testonly != None:
        test_only = testonly
    return cc_binary(
        name = name,
        srcs = srcs,
        hdrs = hdrs,
        private_hdrs = private_hdrs,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        static = static,
        _c = True,
    )

def c_test(
        name,
        srcs = [],
        hdrs = [],
        compiler_flags = [],
        cflags = None,
        copts = None,
        linker_flags = [],
        ldflags = None,
        linkopts = None,
        pkg_config_libs = [],
        pkg_config_cflags = [],
        deps = [],
        worker = "",
        data = [],
        visibility = None,
        flags = "",
        labels = [],
        tags = None,
        features = None,
        flaky = 0,
        test_outputs = None,
        size = None,
        timeout = 0,
        container = False,
        sandbox = None):
    """Defines a C test target.

    Note that you must supply your own main() and test framework (ala cc_test when
    write_main=False).

    Args:
      name (str): Name of the rule
      srcs (list): C or C++ source files to compile.
      hdrs (list): Header files.
      compiler_flags (list): Flags to pass to the compiler.
      linker_flags (list): Flags to pass to the linker.
      pkg_config_libs (list): Libraries to declare a dependency on using pkg-config
      pkg_config_cflags (list): Libraries to declare a dependency on using `pkg-config --cflags`
      deps (list): Dependent rules.
      data (list): Runtime data files for this test.
      visibility (list): Visibility declaration for this rule.
      flags (str): Flags to apply to the test invocation.
      labels (list): Labels to attach to this test.
      flaky (bool | int): If true the test will be marked as flaky and automatically retried.
      test_outputs (list): Extra test output files to generate from this test.
      size (str): Test size (enormous, large, medium or small).
      timeout (int): Length of time in seconds to allow the test to run for before killing it.
      container (bool | dict): If true the test is run in a container (eg. Docker).
      sandbox (bool): Sandbox the test on Linux to restrict access to namespaces such as network.
    """
    if cflags != None:
        compiler_flags = cflags
    if copts != None:
        compiler_flags = copts
    if ldflags != None:
        linker_flags = ldflags
    if linkopts != None:
        linker_flags = linkopts
    if features != None:
        labels = features
    if tags != None:
        labels = tags
    return cc_test(
        name = name,
        srcs = srcs,
        hdrs = hdrs,
        deps = deps,
        worker = worker,
        visibility = visibility,
        compiler_flags = compiler_flags,
        linker_flags = linker_flags,
        pkg_config_libs = pkg_config_libs,
        pkg_config_cflags = pkg_config_cflags,
        data = data,
        flags = flags,
        labels = labels,
        flaky = flaky,
        test_outputs = test_outputs,
        size = size,
        timeout = timeout,
        container = container,
        sandbox = sandbox,
        _c = True,
        write_main = False,
    )

def c_embed_binary(name, src, deps = [], visibility = None, test_only = False):
    """Build rule to embed an arbitrary binary file into a C library.

    You can depend on the output of this as though it were a c_library rule.
    There are five functions available to access the data once compiled, all of which are
    prefixed with the file's basename:
      filename_start(): returns a const char* pointing to the beginning of the data.
      filename_end(): returns a const char* pointing to the end of the data.
      filename_size(): returns the length of the data in bytes.
      filename_start_nc(): returns a char* pointing to the beginning of the data.
                           This is a convenience wrapper using const_cast, you should not
                           mutate the contents of the returned pointer.
      filename_end_nc(): returns a char* pointing to the end of the data.
                         Again, don't mutate the contents of the pointer.
    You don't own the contents of any of these pointers so don't try to delete them :)

    Args:
      name (str): Name of the rule.
      src (str): Source file to embed.
      deps (list): Dependencies.
      visibility (list): Rule visibility.
      test_only (bool): If True, is only available to test rules.
    """
    return cc_embed_binary(
        name = name,
        src = src,
        deps = deps,
        visibility = visibility,
        test_only = test_only,
        _c = True,
    )
